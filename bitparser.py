import time
import logging
import csv
import re
import os.path
import threading
from datetime import date

import requests
from requests.exceptions import RequestException
import bs4

import config

logging.basicConfig(filename='parser.log',
                    level=logging.INFO,
                    format="%(asctime)s:%(levelname)s:%(message)s")

class parser_thread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = name

	def run(self):
		start_parse(self.name)

def make_request(url): 
	try:
		response = requests.get(url,headers=config.HEADERS)
		if response.status_code == 200:
			return response.text
		else:
			return None
	except RequestException:
		logging.error('Request error on url %s' % url)
		return None

def get_html(html):
	if html:
		result = bs4.BeautifulSoup(html, 'lxml')
		return result
	else:
		logging.error('Empty response')
		return None

def get_links(html,link_set,par):
	tag = par['links_to_art']['tag']
	attr = par['links_to_art']['attr']
	attr_value = par['links_to_art']['attr_value']
	short_link = par['short_link']
	urls = []
	to_save = []
	links = html.findAll(tag,{attr: attr_value })
	if links:
		for link in links:
			same_site = re.match(par['browse_url'],link.a['href'])
			if short_link:
				urls.append(link.a['href'][1:])
			elif same_site:
				url = config.link_pattern.findall(link.a['href'])
				urls.append(url[0])
			else:
				continue
	urls = list(set(urls) - link_set) 
	for url in urls:
			time.sleep(1)
			to_save.append(parse_article(url,par))
	write_csv(to_save,par)

def parse_article(url,par):
	url = ''.join([par['browse_url'],url])
	result = []
	article = get_html(make_request(url))
	if article:
		result.append(repr(url))
		result.append(repr(get_title(article,par)))
		result.append(repr(get_text(article,par)))
		result.append(date.today())
		return result
	else:
		return None

def get_title(html,par):
	tag = par['title']['tag']
	attr = par['title']['attr']
	attr_value = par['title']['attr_value']
	head = html.find(tag,{attr: attr_value})
	try:
		return head.text.strip()
	except AttributeError:
		logging.error('title not found')
		return None

def get_text(html,par):
	tag = par['body']['tag']
	attr = par['body']['attr']
	attr_value = par['body']['attr_value']
	result = []
	text_tags = html.find(tag,{attr: attr_value})
	if 'text_func' in par:
		func = globals()[par['text_func']]
		text_tags = func(text_tags)	
	if not text_tags:
		return None
	for i in text_tags.contents:
		if not i.name:
			continue
		if i.name == 'p':
			result.append(parse_paragraph(i))
		if re.match(r'^h\d',i.name):
			result.append('\n'.join([' ',i.text,' ']))
		if i.name == 'blockquote':
			if i.findAll('a'):
				result.append('{ %s }:{ %s }' % (i.p.text,i.findAll('a')[-1]['href']))
			else:
				result.append(i.text)
	return '\n'.join(result)

def btcnb_text(tag):
	"""обработка текста статьи для bitcoinnewsbot"""
	text_tags = tag
	for tag in tag.contents:
		if tag.name == 'div':
			if not tag.attrs:
				text_tags = tag
			elif 'id' in tag.attrs and \
				  tag.attrs['id'].startswith('post-'):
				 tmp_tag = tag.find('div',{'class': 'entry-content'})
				 if tmp_tag:
				 	text_tags = tmp_tag
				 else:
				 	text_tags = tag
			elif 'itemprop' in tag.attrs and \
				 tag.attrs['itemprop'] == 'articleBody':
				 text_tags = tag
	return text_tags

def parse_paragraph(tag):
	result = []
	for tags in tag.contents:
		if tags.name == 'br':
			result.append('\n')
		elif tags.name == 'a':
			try:
				result.append('{%s}:{%s}' % (tags.text,tags['href']))
			except KeyError:
				result.append(tags.text)
		elif hasattr(tags,'text'):
			result.append(tags.text)
		elif tags.name != 'img':
			result.append(tags.string.strip())
	return ' '.join(result)

def read_csv(par):
	file_name = par['csv_name']
	try:
		with open(file_name,'r',encoding='utf-8') as file:
			reader = csv.reader(file,delimiter=',',quoting=csv.QUOTE_ALL)
			if not reader:
				return None
			else:
				links = set()
				reader.__next__()
				for row in reader:
					url = config.link_pattern.findall(row[0].strip("'"))
					links.add(url[0])
				return links
	except FileNotFoundError:
		logging.error('CSV file not found %s' % file_name)

def write_csv(data,par,new=False):
	file_name = par['csv_name']
	with open(file_name,'a',encoding='utf-8',newline='') as file:
		writer = csv.writer(file,delimiter=',',quoting=csv.QUOTE_ALL)
		if new:
			logging.info('Create new csv file')
			writer.writerow(data)
			return
		for row in data:
			if not row:
				return
			if None in row:
				return
			else:
				writer.writerow(row)
		logging.info('%s new records in %s' % (len(data),file_name))

def start_parse(params):
	par = config.params[params]
	if not os.path.exists(par['csv_name']):
		write_csv(['url','title','text','date'],par,True)
	for url in par['urls']:
		resp = make_request(url)
		if resp:
			html = get_html(resp)
			get_links(html,read_csv(par),par)

if __name__ == '__main__':
	print('Scrapping module')