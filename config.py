import re

#заголовки запроса, прикидываемся браузером
HEADERS = {'user-agent':'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0'} 

"""словарь с параметрами парсинга для каждого сайта

ключ словаря (например ctlgrph)- идентефикатор для парсинга конкретного сайта

browse_url - адрес сайта, для сравнения ссылок и выполнения запросов
urls - массив ссылок для просмотра ссылок на статьи
link_to_art - словарь с параметрами тэгов для извлечения ссылок
	tag - тэг
	attr - аттрибут тэга
	attr_value - значение аттрибута тэга
title - словарь с параметрами тэгa для извлечения заголовка
	tag - тэг
	attr - аттрибут тэга
	attr_value - значение аттрибута тэга
body - словарь с параметрами тэгa для извлечения заголовка
	tag - тэг
	attr - аттрибут тэга
	attr_value - значение аттрибута тэга
csv_name - имя файла для сохранение результатов
short_link - True, если ссылки извлекаются без полного адреса

text_func - необязательный ключ, название функции для
	извлечения текста с особыми или неоднородными параметрами.
	Например для http://bitnewsbot.com/
"""

params = {'ctlgrph': 
           {'browse_url': 'https://cointelegraph.com/',
		   'urls': ['https://www.cointelegraph.com'],
		   'links_to_art': {'tag':'div','attr': 'class','attr_value': 'post boxed '},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'header'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'post-full-text contents'},
		   'csv_name': 'ctlgrph.csv',
		   'short_link': False},
		  'coindesk': 
           {'browse_url': 'https://www.coindesk.com/',
		   'urls': ['https://www.coindesk.com/category/technology-news/',
		   			'https://www.coindesk.com/category/markets-news/',
		   			'https://www.coindesk.com/category/business-news/'
		   			],
		   'links_to_art': {'tag':'div','attr': 'id','attr_value': re.compile(r'^post-')},
		   'title': {'tag': 'h3','attr': 'class','attr_value': 'featured-article-title'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'article-content-container'},
		   'csv_name': 'coindesk.csv',
		   'short_link': False
		   },
		  'btcmgzn': 
           {'browse_url': 'https://bitcoinmagazine.com/',
		   'urls': ['https://bitcoinmagazine.com/articles/',
		   			'https://bitcoinmagazine.com/articles/?page=2',
		   			'https://bitcoinmagazine.com/articles/?page=3'
		   			],
		   'links_to_art': {'tag':'div','attr': 'class','attr_value': 'col-lg-11'},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'article--headline'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'rich-text'},
		   'short_link': True,
		   'csv_name': 'btcmgzn.csv'
		   },
		  'ccn': 
           {'browse_url': 'https://www.cryptocoinsnews.com/',
		   'urls': ['https://www.cryptocoinsnews.com/news/',
		   			'https://www.cryptocoinsnews.com/news/page/2/',
		   			'https://www.cryptocoinsnews.com/news/page/3/'
		   			],
		   'links_to_art': {'tag':'h3','attr': 'class','attr_value': 'entry-title'},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'entry-title'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'entry-content'},
		   'short_link': False,
		   'csv_name': 'ccn.csv'
		   },
		  'bitnewsb':
		   {'browse_url': 'http://bitnewsbot.com/',
		   'urls': ['http://bitnewsbot.com/'],
		   'links_to_art': {'tag':'div','attr': 'class','attr_value': 'masonry-item-box'},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'entry-title'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'entry-content'},
		   'short_link': False,
		   'text_func': 'btcnb_text',
		   'csv_name': 'btnewsb.csv'
		   },
		  'forklog':
		   {'browse_url': 'https://forklog.com/',
		   'urls': ['https://forklog.com/news/'],
		   'links_to_art': {'tag':'article','attr': 'id','attr_value': re.compile(r'^post-')},
		   'title': {'tag': 'h1','attr': None,'attr_value': None},
		   'body' : {'tag': 'section','attr': 'id', 'attr_value': 'article_content'},
		   'short_link': False,
		   'csv_name': 'forklog.csv'
		   },
		  'altcoint':
		   {'browse_url': 'http://www.altcointoday.com/',
		   'urls': ['http://www.altcointoday.com/category/news/',
		   		 	'http://www.altcointoday.com/category/news/page/2/',
		   		 	'http://www.altcointoday.com/category/news/page/3/'
		   		 	],
		   'links_to_art': {'tag':'h2','attr': 'class','attr_value': 'entry-title'},
		   'title': {'tag': 'h1','attr': None,'attr_value': None},
		   'body' : {'tag': 'article','attr': 'id', 'attr_value': re.compile(r'^post-')},
		   'short_link': False,
		   'csv_name': 'altcoint.csv'
		   },
		  'coinj':
		   {'browse_url': 'https://coinjournal.net/',
		   'urls': ['https://coinjournal.net/'],
		   'links_to_art': {'tag':'h3','attr': 'class','attr_value': 'entry-title'},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'entry-title'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'td-post-content'},
		   'short_link': False,
		   'csv_name': 'coinj.csv'
		   },
		  'dashfn':
		   {'browse_url': 'https://www.dashforcenews.com/',
		   'urls': ['https://www.dashforcenews.com/'],
		   'links_to_art': {'tag':'h2','attr': 'class','attr_value': 'post-title'},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'entry-title'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'post-content'},
		   'short_link': False,
		   'csv_name': 'dashfn.csv'
		   },
		  'techcrunch':
		   {'browse_url': 'https://techcrunch.com/',
		   'urls': ['https://techcrunch.com/',
		   			'https://techcrunch.com/page/2/',
		   			'https://techcrunch.com/page/3/',
		   			'https://techcrunch.com/page/4/'
		   			],
		   'links_to_art': {'tag':'h2','attr': 'class','attr_value': 'post-title'},
		   'title': {'tag': 'h1','attr': 'class','attr_value': 'tweet-title'},
		   'body' : {'tag': 'div','attr': 'class', 'attr_value': 'article-entry'},
		   'short_link': False,
		   'csv_name': 'techcrunch.csv'
		   }
		 }


link_pattern = re.compile(r'^http.*\.\w*/(.*)')
